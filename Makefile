RESOURCES_DIR := ./kyBoard.bundle/Contents/Resources
DIST_DIR := ./dist
INCLUDE := kyBoard.bundle README.md
TRACK := $(INCLUDE) kyBoard.bundle/**/*

VERSION := $(shell git tag -l | tail -n 1)
FILENAME := $(DIST_DIR)/kyBoard-$(VERSION)

LANGUAGES := English German Russian
ICONS := $(foreach lang,$(LANGUAGES),$(RESOURCES_DIR)/ky$(lang).icns)

.PHONY: all
all: $(FILENAME).zip $(FILENAME).tar.bz2

$(FILENAME).zip: $(TRACK)
	@mkdir -p $(DIST_DIR)
	zip -r9 $@ $(INCLUDE)

$(FILENAME).tar.bz2: $(TRACK)
	@mkdir -p $(DIST_DIR)
	tar --options compression-level=9 -cjf $@ $(INCLUDE)

.PHONY: icons
icons: $(ICONS)

$(RESOURCES_DIR)/%.icns: ./%.iconset/*
	@echo $@
	@echo $^
	iconutil --convert icns --output $@ $<

.PHONY: clean
clean:
	rm -rf $(DIST_DIR)/*
