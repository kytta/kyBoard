kyBoard
=======

A keyboard layout bundle based on the macOS US layout. For English, German,
and Russian.


Inspired by / based off
-----------------------

 - [DeUs-Layout](https://codeberg.org/kytta/kyBoard/src/branch/v2-deus)
   was my first attempt at creating a better German keyboard layout. It's more
   a botch than a end product.
 - [Universal-Layout](https://github.com/tonsky/Universal-Layout/)
   is an attempt to solve the Russian keyboard layout (where the comma is
   in the upper case) by achieving parity with the English counterpart.
   I am using some of the build scripts from here.
 - [Typography Layout](https://ilyabirman.net/typography-layout/)
   is a layout for people who work a lot with texts. Makes typesetting common
   characters a lot easier.
 - [EurKEY](https://eurkey.steffen.bruentjen.eu/)
   is a layout tailored for all European languages. It doesn't suit my taste
   that well since I'd rather have a quicker access to more punctuation
   than to letters I'll never use.


Licence
-------

Copyright © 2022 [Nikita Karamov]\
Licensed under the [BSD 2-Clause "Simplified" License].


---

This project is hosted on Codeberg:
<https://codeberg.org/kytta/kyBoard.git>

[BSD 2-Clause "Simplified" License]: https://spdx.org/licenses/BSD-2-Clause.html
[Nikita Karamov]: https://www.kytta.dev/
